package airCompany.logic;

import java.util.ArrayList;
import java.util.Scanner;
import airCompany.planes.Airliner;
import airCompany.planes.CargoPlane;
import airCompany.planes.Plane;
import static airCompany.ui.UserInterfaceMain.*;

public class AirCompany {
    public static void main(final String[] args) {
        ArrayList<Plane> planes = new ArrayList<Plane>();
        ArrayList<Airliner> airliners = new ArrayList<Airliner>();
        ArrayList<CargoPlane> cargoPlanes = new ArrayList<CargoPlane>();
        initializeAirliners(airliners);
        initializeCargoPlanes(cargoPlanes);
        int passengerSum = countPassengerCap(airliners);
        int cargoSum = countCargoCap(cargoPlanes);
        initializeAirCompany(planes);
        showPlanes(planes);
        menuOut();
        menuIn(planes, cargoSum, passengerSum);
    }

    private static void initializeAirCompany(final ArrayList planes) {
        planes.add(new Airliner("Boeing777", 451, 13936, 0.078));
        planes.add(new Airliner("Boeing777", 451, 13936, 0.078));
        planes.add(new Airliner("AirbusA350", 350, 15600, 0.103));
        planes.add(new CargoPlane("An124", 214000, 7600, 0.044));
        planes.add(new CargoPlane("Boeing Dreamlifter", 113400, 7800, 0.039));
    }

    private static void initializeAirliners(final ArrayList airliners) {
        airliners.add(new Airliner("Boeing777", 451, 13936, 0.078));
        airliners.add(new Airliner("Boeing777", 451, 13936, 0.078));
        airliners.add(new Airliner("AirbusA350", 350, 15600, 0.103));
    }

    private static void initializeCargoPlanes(final ArrayList cargoPlanes) {
        cargoPlanes.add(new CargoPlane("An124", 214000, 7600, 0.044));
        cargoPlanes.add(new CargoPlane("Boeing Dreamlifter", 113400, 7800, 0.039));
    }

    public static int countPassengerCap(final ArrayList<Airliner> airliners) {
        int passengerSum = 0;
        for (Airliner element : airliners) {
            passengerSum += element.getPassengerCap();
        }
        return passengerSum;
    }

    public static int countCargoCap(final ArrayList<CargoPlane> cargoPlanes) {
        int cargoSum = 0;
        for (CargoPlane element : cargoPlanes) {
            cargoSum += element.getCargoCap();
        }
        return cargoSum;
    }

    public static Plane searchPlaneBy(final ArrayList<Plane> planes) {
        Scanner inp = new Scanner(System.in);
        System.out.println("Input Flying range");
        int inRange = inp.nextInt();
        for (Plane element : planes) {
            if (inRange == element.getRange()) {
                System.out.println("Range of " + element.getModel());
                return element;
            } else {
                System.out.println("There are no planes with range like this.");
                return null;
            }
        }
        return null;
    }
}