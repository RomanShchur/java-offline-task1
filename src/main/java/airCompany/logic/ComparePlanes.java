package airCompany.logic;

import airCompany.planes.Plane;

import java.util.Comparator;

public final class ComparePlanes implements Comparator<Plane> {
    public int compare(final Plane a, final Plane b) {
        return a.getRange() - b.getRange();
    }
}