package airCompany.planes;

public final class Airliner extends Plane implements Comparable<Plane> {
    private int passengerCap;
    public Airliner(final String model, final int passengerCap,
                    final int range, final double fuelConsump) {
        this.model = model;
        this.passengerCap = passengerCap;
        this.range = range;
        this.fuelConsump = fuelConsump;
    }
    public int getPassengerCap() {
        return passengerCap;
    }
    public int compareTo(final Plane a) {
        return range - a.getRange();
    }
}