package airCompany.planes;

public final class CargoPlane extends Plane implements Comparable<Plane> {
    private int cargoCap;
    public CargoPlane(final String model, final int cargoCap,
                      final int range, final double fuelConsump) {
        this.model = model;
        this.cargoCap = cargoCap;
        this.range = range;
        this.fuelConsump = fuelConsump;
    }
    public int getCargoCap() {
        return cargoCap;
    }
    public int compareTo(final Plane a) {
        return range - a.getRange();
    }
}