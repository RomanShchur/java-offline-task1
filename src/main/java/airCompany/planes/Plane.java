package airCompany.planes;

public abstract class Plane implements Comparable<Plane> {
    String model;
    int range;
    double fuelConsump;
    public int compareTo(final Airliner a) {
        return range - a.getRange();
    }
    public int getRange() {
        return range;
    }
    public double getFuelConsump() {
        return fuelConsump;
    }
    public String getModel() {
        return model;
    }
}