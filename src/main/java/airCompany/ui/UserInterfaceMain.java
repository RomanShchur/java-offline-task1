package airCompany.ui;

import airCompany.planes.Plane;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

import static airCompany.logic.AirCompany.searchPlaneBy;

enum MenuItems {
    cc,
    sortp,
    searchp,
    q,
    c
}
public class UserInterfaceMain {
    public static void menuOut() {
        System.out.println("\tcc.\tCount passenger and bearing capacities");
        System.out.println("\tsortp.\tSort planes by flying range");
        System.out.println("\tsearchp.\tSearch plane by flying range");
        System.out.println("\tq.\tExit");
        System.out.println("\nSelected option -> ");
    }
    public static void menuIn(final ArrayList<Plane> planes,
                              final int cargoSum, final int passengerSum) {
        Scanner inp = new Scanner(System.in);
        MenuItems menuItems = MenuItems.c;
        while (menuItems != MenuItems.q) {
            try {
                menuOut();
                menuItems = MenuItems.valueOf(inp.nextLine());
                switch (menuItems) {
                    case cc: {
                        System.out.println("Counting capacities");
                        System.out.println(" Passenger cap " +
                                cargoSum + " Cargo cap " + passengerSum);
                        break;
                    }
                    case sortp: {
                        showSortedPlanes(planes);
                        break;
                    }
                    case searchp: {
                        System.out.println("Plane Search");
                        searchPlaneBy(planes);
                        break;
                    }
                    case q:
                        System.out.println("Program ends.");
                        System.exit(0);
                        break;
                    default:
                        System.out.println("Selection out of range.");
                }
            } catch (IllegalArgumentException e) {
                System.out.println("Selection out of range. Try again.");
            }
        }
    }
    public static void showPlanes(final ArrayList<Plane> planes) {
        System.out.println("planes owed by Company:");
        for (Plane element : planes) {
            String model = element.getModel();
            int range = element.getRange();
            double fuelConsump = element.getFuelConsump();
            System.out.println("Plane: " + model + " Range: " +
                    range +  " Fuel consumption: "+ fuelConsump);
        }
    }
    public static void showSortedPlanes(ArrayList<Plane> planes){
        Collections.sort(planes);
        System.out.println("planes sorted by flying range: ");
        for (Plane element : planes) {
            String model = element.getModel();
            int range = element.getRange();
            double fuelConsump = element.getFuelConsump();
            System.out.println("Range: " + range + " Plane: " + model + " Fuel consumption: "+ fuelConsump);
        }
    }
}